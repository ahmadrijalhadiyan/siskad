# SISKAD (Sistem Informasi Scan Kayu Terpadu)

Sistem Informasi Scan Kayu Terpadu

## Getting Started

Aplikasi yang dibuat untuk Scan Barcode Kayu yang ada di lapangan, sesuai dengan standard dan aturan 
dari Kementrian Lingkungan Hidup dan Kehutanan.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
